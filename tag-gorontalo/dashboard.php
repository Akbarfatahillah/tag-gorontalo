  <!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Transportasi Angkasa Gorontalo</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style media="screen">
      /* body{background: #F8F8F8} */

      .topbanner{
        background-blend-mode: overlay;
        background-color: initial!important;
        background-image:
        url(https://www.rentalotojayapura.com/wp-content/uploads/2019/08/holtekamp280219-1.jpg),
        linear-gradient(180deg,#cfffff 0%,#007bff 100%)!important;
        background-repeat: no-repeat;
        background-size: cover;
        }

        #box-card {
          border: 1px solid;
          padding: 10px;
          box-shadow: 5px 10px;
          border-radius: 0px
        }

        .navbar-brand {
          position: absolute;
          width: 100%;
          left: 0;
          top: 0;
          text-align: center;
          margin: auto;
        }

      .navbar-dark .navbar-nav .nav-link {
        color: rgb(255 255 255);
      }

    </style>
  </head>
  <body>

    <div id="home">

      <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="mainNav" style="color: #fff; background: ;">
        <div class="container">

          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="#home">Beranda</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="#list_mobil">Armada Kami</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="#tentang_kami">Tentang Kami</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="#destinasi_wisata">Destinasi Wisata</a>
              </li>
              <li class="nav-item">
                <a class="nav-link js-scroll-trigger" href="#contact">Kontak Kami</a>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <div class="mt-5">
        <div class="topbanner">
          <div class="container">

            <div class="row row-cols-2">
              <div class="row-7">

                <img src="assets/img/banner.png" alt="" class="pt-4 pb-5">
              </div>
              <div class="row-5" style="margin-top: 9em; padding-left: 12em;">
                <p style="font-size: 1.7em; font-weight: 600; line-height: 1.2em">
                  Rental Mobil Jayapura <br>
                  08XX XXXX XXXX <br>
                  Harga Aman Hati Senang <br>
                </p>
                <hr style="width: 80px; border: #007BFF solid 4px; border-radius: 0px; float: left; margin-left: 3em;">
                <br>
                <br>
                <p style="font-size: 1em; font-weight: 600; color: #101010" class="pl-5">
                  Pilihan Terbaik Rental Mobil di Jayapura, <br>
                  Tarif Murah, Layanan Prima, Armada <br>
                  Lengkap dan Bersih, Driver Terpercaya. <br>
                  Hubungi Kami di 08XX - XXXX - XXXX <br>
                </p>

                <a href="#" class="btn btn-primary pt-2 pb-2 pl-3 pr-3 ml-5 mt-3" style="border-radius: 200px; font-weight: 400">Mobil dan Harga Sewa</a>
              </div>
            </div>


          </div>
        </div>
        <div class="container text-center mt-5 mb-5">

          <h1 style="margin-bottom: 1.5em;font-size: 1.7em">Kenapa Sewa Mobil di Transportasi Angkasa Gorontalo</h1>
          <hr style="margin-bottom: 2em;width: 80px; border: #007BFF solid 4px; border-radius: 0px">
          <p style="margin-bottom: 3em;font-weight: 500; color: #404040">Berikut adalah hal - hal yang menjadi pertimbangan anda untuk memilih kami.</p>

          <div class="row row-cols-4">
            <div class="col">
              <img src="assets/icon/icon1.png" alt="">
              <h3 class="text-capitaliz" style="font-size: 1.2em">supir berpengalam</h3>
              <p style="font-weight: 500; color: #404040">Supir yang kami berikan kepada <br> Anda telah berpengalam dan <br> mengenal wilayah Gorontalo </p>
            </div>
            <div class="col">
              <img src="assets/icon/icon2.png" alt="">
              <h3 class="text-capitalize" style="font-size: 1.2em">Kondisi Mobil Terawat</h3>
              <p style="font-weight: 500; color: #404040">Perawatan rutin selalu kami <br> lakukan, yaitu perawatan mobil <br> harian, mingguan bulanan. </p>
            </div>
            <div class="col">
              <img src="assets/icon/icon3.png" alt="">
              <h3 class="text-capitalize" style="font-size: 1.2em">Harga Rental Murah</h3>
              <p style="font-weight: 500; color: #404040">Kami jamin tarif yang kami <br> tawarkan sangat murah <br> kompetitif.</p>
            </div>
            <div class="col">
              <img src="assets/icon/icon4.png" alt="">
              <h3 class="text-capitalize" style="font-size: 1.2em">Proses Sewa Mudah</h3>
              <p style="font-weight: 500; color: #404040">Proses rental dan sewa mobil <br> cukup mudah, hanya telepon atau <br> WA ke contact center kami.</p>
            </div>
          </div>

        </div>
      </div>

    </div>


    <br> <hr> <br>

    <div id="list_mobil">

    <div class="container text-center mt-3 mb-5">
      <h1 style="font-size: 1.7em; margin-bottom: 1.5em;">Armada Kami</h1>
      <hr style="width: 80px; border: #007BFF solid 4px; border-radius: 0px; margin-bottom: 2em;">
      <p style="font-weight: 500; color: #404040">Berikut kendaraan dan Tarif Harga Murah untuk Berbagai Jenis Kendaraan Sesuai Kebutuhan Anda <strong style="color: #000"> <a href="#">(Bisa Nego Sama Admin Loh)</a></strong>. </p>

      <div class="row row-cols-4 mt-5">

        <div class="col">
          <div class="card border-primary mb-3" id="box-card" style="max-width: 18rem;">
            <div class="card-header bg-primary border-primary text-light">TOYOTA AVANZA</div>
            <div class="card-body">
              <img src="https://www.rentalotojayapura.com/wp-content/uploads/2019/08/Mobil-avnza-veloz.png" class="card-img-top" alt="...">
              <hr>
              <h5 class="card-title">IDR 350.000 / 12 Jam</h5>
              <p class="card-text">Harga sudah termasuk driver professional yang akan melayani Anda.</p>
            </div>
          </div>
        </div>

        <div class="col">
          <div class="card border-primary mb-3" id="box-card" style="max-width: 18rem;">
            <div class="card-header bg-primary border-primary text-light">TOYOTA AVANZA</div>
            <div class="card-body">
              <img src="https://www.rentalotojayapura.com/wp-content/uploads/2019/08/Mobil-avnza-veloz.png" class="card-img-top" alt="...">
              <hr>
              <h5 class="card-title">IDR 350.000 / 12 Jam</h5>
              <p class="card-text">Harga sudah termasuk driver professional yang akan melayani Anda.</p>
            </div>
          </div>
        </div>

        <div class="col">
          <div class="card border-primary mb-3" id="box-card" style="max-width: 18rem;">
            <div class="card-header bg-primary border-primary text-light">TOYOTA AVANZA</div>
            <div class="card-body">
              <img src="https://www.rentalotojayapura.com/wp-content/uploads/2019/08/Mobil-avnza-veloz.png" class="card-img-top" alt="...">
              <hr>
              <h5 class="card-title">IDR 350.000 / 12 Jam</h5>
              <p class="card-text">Harga sudah termasuk driver professional yang akan melayani Anda.</p>
            </div>
          </div>
        </div>

        <div class="col">
          <div class="card border-primary mb-3" id="box-card" style="max-width: 18rem;">
            <div class="card-header bg-primary border-primary text-light">TOYOTA AVANZA</div>
            <div class="card-body">
              <img src="https://www.rentalotojayapura.com/wp-content/uploads/2019/08/Mobil-avnza-veloz.png" class="card-img-top" alt="...">
              <hr>
              <h5 class="card-title">IDR 350.000 / 12 Jam</h5>
              <p class="card-text">Harga sudah termasuk driver professional yang akan melayani Anda.</p>
            </div>
          </div>
        </div>

      </div>

      <a href="list_mobil.php" style="float: right" class="pt-4">Daftar Mobil Lain
        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-right" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
        </svg>
      </a>
    </div>

    </div>

    <br> <hr> <br>

    <div id="tentang_kami">

    <div class="container text-center mt-3 mb-5">
      <h1 style="font-size: 1.7em; margin-bottom: 1.5em;">Tentang Kami Transportasi Angkasa Gorontalo</h1>
      <hr style="width: 80px; border: #007BFF solid 4px; border-radius: 0px; margin-bottom: 2em;">
      <p style="font-weight: 500; color: #404040">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
    </div>

    </div>

    <br> <hr> <br>

    <div id="destinasi_wisata">

    <div class="container text-center mt-3 mb-5">
      <h1 style="font-size: 1.7em; margin-bottom: 1.5em;">Destinasi Wisata Gorontalo</h1>
      <hr style="width: 80px; border: #007BFF solid 4px; border-radius: 0px; margin-bottom: 2em;">

      <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
          <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="https://bisniswisata.co.id/wp-content/uploads/2017/05/danau-limboto.jpg" class="d-block w-100" alt="..." height="400px">
            <div class="carousel-caption d-none d-md-block">
              <h5>First slide label</h5>
              <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
            </div>
          </div>
          <div class="carousel-item">
            <img src="https://image.cermati.com/q_70/intqqzkim8vnqc1flq5o" class="d-block w-100" alt="..." height="400px">
            <div class="carousel-caption d-none d-md-block">
              <h5>Second slide label</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>
          </div>
          <div class="carousel-item">
            <img src="https://sgp1.digitaloceanspaces.com/tz-mag-id/wp-content/uploads/2018/07/060607074848/wisata-di-gorontalo-12-1024x683.jpg" class="d-block w-100" alt="..." height="400px">
            <div class="carousel-caption d-none d-md-block">
              <h5>Third slide label</h5>
              <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      <a href="list_wisata.php" style="float: right" class="pt-4">Destinasi Wisata Lain
        <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-arrow-right" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" d="M1 8a.5.5 0 0 1 .5-.5h11.793l-3.147-3.146a.5.5 0 0 1 .708-.708l4 4a.5.5 0 0 1 0 .708l-4 4a.5.5 0 0 1-.708-.708L13.293 8.5H1.5A.5.5 0 0 1 1 8z"/>
        </svg>
      </a>
    </div>

    </div>

    <br> <hr> <br>

    <div id="contact">

    <div class="container">
      <div class="row row-cols-2">
        <div class="col-5">
          <img src="assets/img/contact2.png" alt="">

        </div>
        <div class="col-7 pl-5">
          <h1 style="font-size: 1.7em">Kontak Kami</h1>
          <hr style="width: 80px; border: #007BFF solid 4px; border-radius: 0px; float: left"><br><br>
          <p style="font-size: 1.2em; font-weight: 500">Untuk Pemesanan dan Informasi Lengkap. Silahkan Telepon Kontak Kami di bawah ini.</p>
          <p style="font-size: 1em; font-weight: 500">Segera Nikmati Kenyamanan Berkendara Bersama kami.</p><br>
          <p style="font-size: 1.3em; font-weight: 500">Tlp /WA : 08XX XXXX XXXX</p>
          <p style="font-size: 1em; font-weight: 500">Alamat : Jln.bandara Sultan Hasanudin, Gorontalo</p>
        </div>
      </div>

    </div>

    </div>


    <hr>

    <div class="footer-copyright text-center py-1">
      &copy;2020 Copyright kaizenxpoligon
      <br><br>
    </div>


    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
  </body>
</html>

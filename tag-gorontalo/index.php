<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Transportasi Angkasa Gorontalo</title>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
    <style media="screen">
      body{background: #F8F8F8}
    </style>
  </head>
  <body>

    <nav class="navbar navbar-dark bg-light" style="height: 35px">
      <h2 class="display-4" style="padding-left: 40%; color:#000; font-size: 1.3em">
        Transportasi Angkasa Gorontalo
      </h2>
    </nav>

    <nav class="navbar navbar-expand-lg navbar-dark bg-primary mb-2">
      <a class="navbar-brand" href="#">TAG</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="#">Daftar Mobil</a>
          </li>
          <li class="nav-item active">
            <a class="nav-link" href="#">Destinasi Wisata</a>
          </li>
        </ul>
      </div>
    </nav>

    <div class="container">
      <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="https://www.carbonbrief.org/wp-content/uploads/2019/03/indonesia-hero.jpg" class="d-block w-100" alt="..."  height="400px">
          </div>
          <div class="carousel-item">
            <img src="https://d26lfat00jharn.cloudfront.net/blog/wp-content/uploads/2019/10/Places-to-Visit-in-Indonesia-1024x720.jpg" class="d-block w-100" alt="..." height="400px">
          </div>
          <div class="carousel-item">
            <img src="https://backpackeradvice.com/img/Papua-Barat-Raja-Ampat.jpg" class="d-block w-100" alt="..." height="400px">
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>

    <nav class="navbar navbar-expand-lg navbar-dark bg-primary mt-2">
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
    </nav>

    <div class="container mt-4">

      <div class="row row-cols-4">

        <div class="col">
          <div class="card" style="width: 15rem;">
            <img src="https://www.toyota.astra.co.id/sites/default/files/2020-07/4.%20alphard%20colors%20black.png" class="card-img-top mt-2 mt-2" alt="...">
            <div class="card-body">
              <h5 class="card-title text-center">Alphard</h5>
              <p class="card-text">Rp. 1.500.000 / 12 Jam</p>
            </div>
          </div>
        </div>

        <div class="col">
          <div class="card" style="width: 15rem;">
            <img src="https://www.toyota.astra.co.id/sites/default/files/2020-07/4.%20alphard%20colors%20black.png" class="card-img-top mt-2" alt="...">
            <div class="card-body">
              <h5 class="card-title text-center">Avanza</h5>
              <p class="card-text">Rp. 350.000  / 12 Jam</p>
            </div>
          </div>
        </div>

        <div class="col">
          <div class="card" style="width: 15rem;">
            <img src="https://www.toyota.astra.co.id/sites/default/files/2020-07/4.%20alphard%20colors%20black.png" class="card-img-top mt-2" alt="...">
            <div class="card-body">
              <h5 class="card-title">Innova</h5>
              <p class="card-text">Rp. 450.000/ 12 Jam</p>
            </div>
          </div>
        </div>

        <div class="col">
          <div class="card" style="width: 15rem;">
            <img src="https://www.toyota.astra.co.id/sites/default/files/2020-07/4.%20alphard%20colors%20black.png" class="card-img-top mt-2" alt="...">
            <div class="card-body">
              <h5 class="card-title">Hiace</h5>
              <p class="card-text">Rp. 1.000.000/ 12 Jam</p>
            </div>
          </div>
        </div>

        <div class="col-12 mt-4"> </div>

        <div class="col">
          <div class="card" style="width: 15rem;">
            <img src="https://www.toyota.astra.co.id/sites/default/files/2020-07/4.%20alphard%20colors%20black.png" class="card-img-top mt-2" alt="...">
            <div class="card-body">
              <h5 class="card-title">Pajero Sport</h5>
              <p class="card-text">Rp. 900.000/ 12 Jam</p>
            </div>
          </div>
        </div>

        <div class="col">
          <div class="card" style="width: 15rem;">
            <img src="https://www.toyota.astra.co.id/sites/default/files/2020-07/4.%20alphard%20colors%20black.png" class="card-img-top mt-2" alt="...">
            <div class="card-body">
              <h5 class="card-title">Fortuner</h5>
              <p class="card-text">Rp. 900.000/ 12 Jam</p>
            </div>
          </div>
        </div>

        <div class="col">
          <div class="card" style="width: 15rem;">
            <img src="https://www.toyota.astra.co.id/sites/default/files/2020-07/4.%20alphard%20colors%20black.png" class="card-img-top mt-2" alt="...">
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              <p class="card-text">Rp. / 12 Jam</p>
            </div>
          </div>
        </div>

        <div class="col">
          <div class="card" style="width: 15rem;">
            <img src="https://www.toyota.astra.co.id/sites/default/files/2020-07/4.%20alphard%20colors%20black.png" class="card-img-top mt-2" alt="...">
            <div class="card-body">
              <h5 class="card-title">Card title</h5>
              <p class="card-text">Rp. / 12 Jam</p>
            </div>
          </div>
        </div>

    </div>
    </div>

    <div class="row mt-4" style="color:#fff">
      <div class="col-4  bg-primary">
        <h1 class=" pl-5 pt-2">
          Contact US
        </h1>
        <p class="pl-4">
          ig @transportasiangkasagorontalo
          <br>
          fb @transportasiangkasagorontalo
          <br>
          wa 08xx - xxxx - xxxx
          <br>
          telp 08xx - xxxx - xxxx
        </p>
      </div>
      <div class="col-1"></div>
      <div class="col-7 bg-primary">
        <h1 class="display-4">Transportasi Angkasa Gorontalo</h1>
        <p>
          <p>Jika anda mencari Sewa Mobil Manado, kamilah tempat yang tepat. Kami menyediakan sewa mobil di Manado plus driver, sewa mobil Manado plus driver dan bbm, serta sewa mobil manado lepas kunci atau self drive.

            Di Manado Kami tidak saja menyewakan, menyediakan mobil bertransmisi manual tapi juga menyewakan mobil bertransmisi matic.</p>
        </p>
      </div>

    </div>



    <div class="navbar navbar-dark bg-primary pt-5 mt-5" style="padding-left: 10%;padding-right: 10%;">
      <h3 class="navbar-brand text-uppercase display-1" color="#fff">
        Destinasi Wisata
      </h3>
      <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
          <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="https://www.carbonbrief.org/wp-content/uploads/2019/03/indonesia-hero.jpg" class="d-block w-100" alt="..." height="350px">
            <div class="carousel-caption d-none d-md-block">
              <h5>First slide label</h5>
              <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
            </div>
          </div>
          <div class="carousel-item">
            <img src="https://d26lfat00jharn.cloudfront.net/blog/wp-content/uploads/2019/10/Places-to-Visit-in-Indonesia-1024x720.jpg" class="d-block w-100" alt="..." height="350px">
            <div class="carousel-caption d-none d-md-block">
              <h5>Second slide label</h5>
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>
          </div>
          <div class="carousel-item">
            <img src="https://backpackeradvice.com/img/Papua-Barat-Raja-Ampat.jpg" class="d-block w-100" alt="..." height="350px">
            <div class="carousel-caption d-none d-md-block">
              <h5>Third slide label</h5>
              <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
      <a href="#">Lihat Daftar Wisata</a>
    </div>

    <div class="container-fluid mt-5" style="color:#fff">
      <div class="bg-primary pt-4 pr-3 pl-3">
        <div class="row row-cols-3  pb-5 text-center">
          <div class="col pb-3">
            <svg width="5em" height="5em" viewBox="0 0 16 16" class="bi bi-geo-alt-fill text-light" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10zm0-7a3 3 0 1 0 0-6 3 3 0 0 0 0 6z"/>
            </svg>
          </div>
          <div class="col pb-3">
            <svg width="5em" height="5em" viewBox="0 0 16 16" class="bi bi-globe text-light" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm7.5-6.923c-.67.204-1.335.82-1.887 1.855A7.97 7.97 0 0 0 5.145 4H7.5V1.077zM4.09 4H2.255a7.025 7.025 0 0 1 3.072-2.472 6.7 6.7 0 0 0-.597.933c-.247.464-.462.98-.64 1.539zm-.582 3.5h-2.49c.062-.89.291-1.733.656-2.5H3.82a13.652 13.652 0 0 0-.312 2.5zM4.847 5H7.5v2.5H4.51A12.5 12.5 0 0 1 4.846 5zM8.5 5v2.5h2.99a12.495 12.495 0 0 0-.337-2.5H8.5zM4.51 8.5H7.5V11H4.847a12.5 12.5 0 0 1-.338-2.5zm3.99 0V11h2.653c.187-.765.306-1.608.338-2.5H8.5zM5.145 12H7.5v2.923c-.67-.204-1.335-.82-1.887-1.855A7.97 7.97 0 0 1 5.145 12zm.182 2.472a6.696 6.696 0 0 1-.597-.933A9.268 9.268 0 0 1 4.09 12H2.255a7.024 7.024 0 0 0 3.072 2.472zM3.82 11H1.674a6.958 6.958 0 0 1-.656-2.5h2.49c.03.877.138 1.718.312 2.5zm6.853 3.472A7.024 7.024 0 0 0 13.745 12H11.91a9.27 9.27 0 0 1-.64 1.539 6.688 6.688 0 0 1-.597.933zM8.5 12h2.355a7.967 7.967 0 0 1-.468 1.068c-.552 1.035-1.218 1.65-1.887 1.855V12zm3.68-1h2.146c.365-.767.594-1.61.656-2.5h-2.49a13.65 13.65 0 0 1-.312 2.5zm2.802-3.5h-2.49A13.65 13.65 0 0 0 12.18 5h2.146c.365.767.594 1.61.656 2.5zM11.27 2.461c.247.464.462.98.64 1.539h1.835a7.024 7.024 0 0 0-3.072-2.472c.218.284.418.598.597.933zM10.855 4H8.5V1.077c.67.204 1.335.82 1.887 1.855.173.324.33.682.468 1.068z"/>
            </svg>
          </div>
          <div class="col pb-3">
            <svg width="5em" height="5em" viewBox="0 0 16 16" class="bi bi-file-earmark-person-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M2 2a2 2 0 0 1 2-2h5.293A1 1 0 0 1 10 .293L13.707 4a1 1 0 0 1 .293.707V14a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2zm7.5 1.5v-2l3 3h-2a1 1 0 0 1-1-1zM11 8a3 3 0 1 1-6 0 3 3 0 0 1 6 0zm2 5.755S12 12 8 12s-5 1.755-5 1.755V14a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1v-.245z"/>
            </svg>
          </div>
          </center>

          <div class="col">
            Alamat <br>
            Jalan Adipura Raya No. 24 Linkungan III, <br>
            Kima Atas, Mapanget, Kota Manado,
            Sulawesi Utara 95259
          </div>
          <div class="col">
            Telepon <br>
            Marketing : +628114331414 <br>
            Support    : +6285395651122 <br>
          </div>
          <div class="col">
            Web & Email <br>
            https://smartrentalmobil.com <br>
            cs@smartrentalmobil.com <br>
            info@smartrentalmobil.com <br>
          </div>
        </div>
      </div>
    </div>

    <div class="bg-primary" style="height: 60px; width: 100%; border: #fff solid 1px;">
      <center>
      <p style="float:center; color: #fff" class="pt-3">&copy;2020 Poligon X Kaizen</p>
    </div>

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
  </body>
</html>

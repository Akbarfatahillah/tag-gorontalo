<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
  <meta charset="utf-8">
  <title>Transportasi Angkasa Gorontalo</title>

  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
  <style media="screen">
    /* body{background: #F8F8F8} */

    .topbanner{
      background-blend-mode: overlay;
      background-color: initial!important;
      background-image:
      url(https://www.rentalotojayapura.com/wp-content/uploads/2019/08/holtekamp280219-1.jpg),
      linear-gradient(180deg,#cfffff 0%,#007bff 100%)!important;
      background-repeat: no-repeat;
      background-size: cover;
      }

      #box-card {
        border: 1px solid;
        padding: 10px;
        box-shadow: 5px 10px;
        border-radius: 0px
      }

      .navbar-brand {
        position: absolute;
        width: 100%;
        left: 0;
        top: 0;
        text-align: center;
        margin: auto;
      }

    .navbar-dark .navbar-nav .nav-link {
      color: rgb(255 255 255);
    }

  </style>
</head>
<body>

  <div id="home">

    <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="mainNav" style="color: #fff; background: ;">
      <div class="container">

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#home">Beranda</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#list_mobil">Armada Kami</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#tentang_kami">Tentang Kami</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#destinasi_wisata">Destinasi Wisata</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Kontak Kami</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>

  <br> <hr> <br>

  <div id="list_mobil">

  <div class="container text-center mt-3 mb-3">
    <h1 style="font-size: 1.7em; margin-bottom: 1.3em;">Armada Kami</h1>
    <hr style="width: 80px; border: #007BFF solid 4px; border-radius: 0px; margin-bottom: 2em;">

    

    <div class="row row-cols-4 mt-3">

      <div class="col">
        <div class="card border-primary mb-3" id="box-card" style="max-width: 18rem;">
          <div class="card-header bg-primary border-primary text-light">TOYOTA AVANZA</div>
          <div class="card-body">
            <img src="https://www.rentalotojayapura.com/wp-content/uploads/2019/08/Mobil-avnza-veloz.png" class="card-img-top" alt="...">
            <hr>
            <h5 class="card-title">IDR 350.000 / 12 Jam</h5>
            <p class="card-text">Harga sudah termasuk driver professional yang akan melayani Anda.</p>
          </div>
        </div>
      </div>

      <div class="col">
        <div class="card border-primary mb-3" id="box-card" style="max-width: 18rem;">
          <div class="card-header bg-primary border-primary text-light">TOYOTA AVANZA</div>
          <div class="card-body">
            <img src="https://www.rentalotojayapura.com/wp-content/uploads/2019/08/Mobil-avnza-veloz.png" class="card-img-top" alt="...">
            <hr>
            <h5 class="card-title">IDR 350.000 / 12 Jam</h5>
            <p class="card-text">Harga sudah termasuk driver professional yang akan melayani Anda.</p>
          </div>
        </div>
      </div>

      <div class="col">
        <div class="card border-primary mb-3" id="box-card" style="max-width: 18rem;">
          <div class="card-header bg-primary border-primary text-light">TOYOTA AVANZA</div>
          <div class="card-body">
            <img src="https://www.rentalotojayapura.com/wp-content/uploads/2019/08/Mobil-avnza-veloz.png" class="card-img-top" alt="...">
            <hr>
            <h5 class="card-title">IDR 350.000 / 12 Jam</h5>
            <p class="card-text">Harga sudah termasuk driver professional yang akan melayani Anda.</p>
          </div>
        </div>
      </div>

      <div class="col">
        <div class="card border-primary mb-3" id="box-card" style="max-width: 18rem;">
          <div class="card-header bg-primary border-primary text-light">TOYOTA AVANZA</div>
          <div class="card-body">
            <img src="https://www.rentalotojayapura.com/wp-content/uploads/2019/08/Mobil-avnza-veloz.png" class="card-img-top" alt="...">
            <hr>
            <h5 class="card-title">IDR 350.000 / 12 Jam</h5>
            <p class="card-text">Harga sudah termasuk driver professional yang akan melayani Anda.</p>
          </div>
        </div>
      </div>

      <div class="col mt-3">
        <div class="card border-primary mb-3" id="box-card" style="max-width: 18rem;">
          <div class="card-header bg-primary border-primary text-light">TOYOTA AVANZA</div>
          <div class="card-body">
            <img src="https://www.rentalotojayapura.com/wp-content/uploads/2019/08/Mobil-avnza-veloz.png" class="card-img-top" alt="...">
            <hr>
            <h5 class="card-title">IDR 350.000 / 12 Jam</h5>
            <p class="card-text">Harga sudah termasuk driver professional yang akan melayani Anda.</p>
          </div>
        </div>
      </div>

      <div class="col mt-3">
        <div class="card border-primary mb-3" id="box-card" style="max-width: 18rem;">
          <div class="card-header bg-primary border-primary text-light">TOYOTA AVANZA</div>
          <div class="card-body">
            <img src="https://www.rentalotojayapura.com/wp-content/uploads/2019/08/Mobil-avnza-veloz.png" class="card-img-top" alt="...">
            <hr>
            <h5 class="card-title">IDR 350.000 / 12 Jam</h5>
            <p class="card-text">Harga sudah termasuk driver professional yang akan melayani Anda.</p>
          </div>
        </div>
      </div>

      <div class="col mt-3">
        <div class="card border-primary mb-3" id="box-card" style="max-width: 18rem;">
          <div class="card-header bg-primary border-primary text-light">TOYOTA AVANZA</div>
          <div class="card-body">
            <img src="https://www.rentalotojayapura.com/wp-content/uploads/2019/08/Mobil-avnza-veloz.png" class="card-img-top" alt="...">
            <hr>
            <h5 class="card-title">IDR 350.000 / 12 Jam</h5>
            <p class="card-text">Harga sudah termasuk driver professional yang akan melayani Anda.</p>
          </div>
        </div>
      </div>

      <div class="col mt-3">
        <div class="card border-primary mb-3" id="box-card" style="max-width: 18rem;">
          <div class="card-header bg-primary border-primary text-light">TOYOTA AVANZA</div>
          <div class="card-body">
            <img src="https://www.rentalotojayapura.com/wp-content/uploads/2019/08/Mobil-avnza-veloz.png" class="card-img-top" alt="...">
            <hr>
            <h5 class="card-title">IDR 350.000 / 12 Jam</h5>
            <p class="card-text">Harga sudah termasuk driver professional yang akan melayani Anda.</p>
          </div>
        </div>
      </div>

      <div class="col mt-3">
        <div class="card border-primary mb-3" id="box-card" style="max-width: 18rem;">
          <div class="card-header bg-primary border-primary text-light">TOYOTA AVANZA</div>
          <div class="card-body">
            <img src="https://www.rentalotojayapura.com/wp-content/uploads/2019/08/Mobil-avnza-veloz.png" class="card-img-top" alt="...">
            <hr>
            <h5 class="card-title">IDR 350.000 / 12 Jam</h5>
            <p class="card-text">Harga sudah termasuk driver professional yang akan melayani Anda.</p>
          </div>
        </div>
      </div>

      <div class="col mt-3">
        <div class="card border-primary mb-3" id="box-card" style="max-width: 18rem;">
          <div class="card-header bg-primary border-primary text-light">TOYOTA AVANZA</div>
          <div class="card-body">
            <img src="https://www.rentalotojayapura.com/wp-content/uploads/2019/08/Mobil-avnza-veloz.png" class="card-img-top" alt="...">
            <hr>
            <h5 class="card-title">IDR 350.000 / 12 Jam</h5>
            <p class="card-text">Harga sudah termasuk driver professional yang akan melayani Anda.</p>
          </div>
        </div>
      </div>

      <div class="col mt-3">
        <div class="card border-primary mb-3" id="box-card" style="max-width: 18rem;">
          <div class="card-header bg-primary border-primary text-light">TOYOTA AVANZA</div>
          <div class="card-body">
            <img src="https://www.rentalotojayapura.com/wp-content/uploads/2019/08/Mobil-avnza-veloz.png" class="card-img-top" alt="...">
            <hr>
            <h5 class="card-title">IDR 350.000 / 12 Jam</h5>
            <p class="card-text">Harga sudah termasuk driver professional yang akan melayani Anda.</p>
          </div>
        </div>
      </div>

      <div class="col mt-3">
        <div class="card border-primary mb-3" id="box-card" style="max-width: 18rem;">
          <div class="card-header bg-primary border-primary text-light">TOYOTA AVANZA</div>
          <div class="card-body">
            <img src="https://www.rentalotojayapura.com/wp-content/uploads/2019/08/Mobil-avnza-veloz.png" class="card-img-top" alt="...">
            <hr>
            <h5 class="card-title">IDR 350.000 / 12 Jam</h5>
            <p class="card-text">Harga sudah termasuk driver professional yang akan melayani Anda.</p>
          </div>
        </div>
      </div>


    </div>
  </div>

  </div>

  <br> <hr> <br>

  <div id="contact">

  <div class="container">
    <div class="row row-cols-2">
      <div class="col-5">
        <img src="assets/img/contact2.png" alt="">

      </div>
      <div class="col-7 pl-5">
        <h1 style="font-size: 1.7em">Kontak Kami</h1>
        <hr style="width: 80px; border: #007BFF solid 4px; border-radius: 0px; float: left"><br><br>
        <p style="font-size: 1.2em; font-weight: 500">Untuk Pemesanan dan Informasi Lengkap. Silahkan Telepon Kontak Kami di bawah ini.</p>
        <p style="font-size: 1em; font-weight: 500">Segera Nikmati Kenyamanan Berkendara Bersama kami.</p><br>
        <p style="font-size: 1.3em; font-weight: 500">Tlp /WA : 08XX XXXX XXXX</p>
        <p style="font-size: 1em; font-weight: 500">Alamat : Jln.bandara Sultan Hasanudin, Gorontalo</p>
      </div>
    </div>

  </div>

  </div>


  <hr>

  <div class="footer-copyright text-center py-1">
    &copy;2020 Copyright kaizenxpoligon
    <br><br>
  </div>


  <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
</body>
</html>
